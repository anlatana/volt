module Api
    module V1
        class HardwaresController < ApplicationController
            protect_from_forgery except: :index
            skip_before_action :verify_authenticity_token

            def index
              hardwares = Hardware.order('created_at DESC')
              render json: {status: 'SUCCESS', data: hardwares}, status: :ok  
            end

            def show
                hardware = Hardware.find(params[:id])
                render json: {status: 'SUCCESS', data: hardware}, status: :ok  
            end

            def create
                hardware = Hardware.new(hardware_params)

                if hardware.save
                    render json: {status: 'SUCCESS', data: hardware}, status: :ok  
                else
                    render json: {status: 'ERROR', data: hardware}, status: :notok   
                end
            end

            def destroy
                hardware = Hardware.find(params[:id])
                hardware.destroy
                render json: {status: 'SUCCESS', data: hardware}, status: :ok  
            end

            def update
                hardware = Hardware.find(params[:id])
                if hardware.update_attributes(hardware_params)
                    render json: {status: 'SUCCESS', data: hardware}, status: :ok  
                else
                    render json: {status: 'ERROR', data: hardware}, status: :notok  
                end
            end

            private
            def hardware_params
                params.permit(:vendor, :product)
            end
        end     
    end
end 
