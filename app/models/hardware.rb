class Hardware < ApplicationRecord
    validates :vendor, presence: true
    validates :product, presence: true
end
