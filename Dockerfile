FROM ruby:2.5.0
RUN apt-get update -qq && apt-get install -y \
  build-essential \
  libpq-dev nodejs
RUN mkdir /volt
WORKDIR /volt
COPY Gemfile /volt/Gemfile
RUN touch /volt/Gemfile.lock
RUN bundle install
COPY . /volt
