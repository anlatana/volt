class CreateHardwares < ActiveRecord::Migration[5.2]
  def up
    create_table :hardwares do |t|
      t.string :vendor
      t.string :modul
 
      t.timestamps
    end

    def down
      drop_table :hardwares
    end
  end
end
