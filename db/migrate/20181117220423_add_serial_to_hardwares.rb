class AddSerialToHardwares < ActiveRecord::Migration[5.2]
  class Hardware < ActiveRecord::Base
  end
  
  def change
    add_column :hardwares, :serial, :string
    Hardware.reset_column_information
  end
end
