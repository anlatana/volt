class RenameModulFromHardwares < ActiveRecord::Migration[5.2]
  class Hardware < ActiveRecord::Base
  end

  def change
    rename_column :hardwares, :modul, :product
    Hardware.reset_column_information
  end
end